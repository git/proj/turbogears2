# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils

MY_PN="z3c.recipe.sphinxdoc"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Use Sphinx to build documentation for zope.org."
HOMEPAGE="http://pypi.python.org/pypi/z3c.recipe.sphinxdoc/"
SRC_URI="http://pypi.python.org/packages/source/z/${MY_PN}/${MY_P}.tar.gz"
LICENSE="ZPL"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="net-zope/zc-buildout
	net-zope/zc-recipe-egg
	dev-python/sphinx"
DEPEND="dev-python/setuptools
	dev-python/docutils"

S="${WORKDIR}/${MY_P}"
