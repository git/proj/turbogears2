# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit versionator

DESCRIPTION="TurboGears 2 + tg.devtools + virtualenv + quickstart app dependencies"
KEYWORDS="~amd64 ~x86"
SLOT="2"
IUSE=""

QUICKSTART_APP_DEPEND=">=dev-python/turbogears-2.0_beta7
	>=dev-python/catwalk-2.0.2
	>=dev-python/Babel-0.9.4
	>=net-zope/zope-sqlalchemy-0.4
	>=dev-python/repoze-tm2-1.0_alpha4"

RDEPEND=">=dev-python/turbogears-2
	>=dev-python/tg-devtools-2
	dev-python/virtualenv
	${QUICKSTART_APP_DEPEND}"

DEPEND=""
