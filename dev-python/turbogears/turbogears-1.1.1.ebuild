# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/turbogears/turbogears-1.0.8.ebuild,v 1.2 2009/04/01 11:49:33 patrick Exp $

PYTHON_DEPEND="2:2.5"  # no wsgiref in Python 2.4
PYTHON_USE_WITH="test? ( ipv6 )"

inherit distutils

KEYWORDS="~x86 ~amd64"

MY_PN=TurboGears
MY_P=${MY_PN}-${PV}

DESCRIPTION="The rapid web development meta-framework you've been looking for."
HOMEPAGE="http://www.turbogears.org/"
SRC_URI="http://files.turbogears.org/eggs/${MY_P}.tar.gz"
LICENSE="MIT"
SLOT="0"
IUSE="test"

RDEPEND=">=dev-python/turbojson-1.2.1
	>=dev-python/turbocheetah-1.0
	>=dev-python/turbokid-1.0.5
	<dev-python/cherrypy-3
	>=dev-python/simplejson-1.9.1
	>=dev-python/pastescript-1.7
	>=dev-python/formencode-1.2.1
	>=dev-python/ruledispatch-0.5_pre2306
	>=dev-python/decoratortools-1.4
	>=dev-python/configobj-4.3.2
	>=dev-python/cheetah-2.0_rc7-r1
	>=dev-python/celementtree-1.0.5
	>=dev-python/sqlobject-0.10.1
	test? ( >=dev-python/nose-0.9.3
		>=dev-python/sqlalchemy-0.4.3
		>=dev-python/tgmochikit-1.4.2
		dev-python/pysqlite
		>=dev-python/webtest-1.2.3 )
	>=dev-python/genshi-0.3.6
	>=dev-python/toscawidgets-0.9.6
	>=dev-python/tw-forms-0.9.6
	>=dev-python/peak-rules-0.5_alpha1_pre2555"
DEPEND="${RDEPEND}
	app-arch/zip
	>=dev-python/setuptools-0.6.8"

S="${WORKDIR}/${MY_P}"

DOCS="CHANGELOG.txt CONTRIBUTORS.txt"

src_test() {
	#I had this fail if pylons is emerged: can't import turbogears config
	#pythonhead http://trac.turbogears.org/ticket/1774
	PYTHONPATH=. "${python}" setup.py test || die "tests failed"
}

pkg_postinst() {
	elog "While not directly depending on them, TurboGears works with/integrates"
	elog "the following packages:"
	elog " - dev-python/elixir"
	elog " - dev-python/sqlalchemy (already installed when built with tests enabled)"
	elog " - dev-python/tg-widgets-lightbox"
	elog " - dev-python/tg-widgets-scriptaculous"
}
