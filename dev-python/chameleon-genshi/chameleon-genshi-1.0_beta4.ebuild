# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Ebuild generated by g-pypi 0.2.1 (rev. 204)

inherit distutils

MY_PN="${PN/-/.}"
MY_PV="${PV/_beta/b}"
MY_P="${MY_PN}-${MY_PV}"

DESCRIPTION="Genshi template engine based on Chameleon"
HOMEPAGE="UNKNOWN"
SRC_URI="http://pypi.python.org/packages/source/c/${MY_PN}/${MY_P}.tar.gz"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="net-zope/zope-interface
	net-zope/zope-component
	>=net-zope/zope-i18n-3.5
	>=dev-python/chameleon-core-1.0_beta21"
DEPEND="dev-python/setuptools"

S="${WORKDIR}/${MY_P}"
