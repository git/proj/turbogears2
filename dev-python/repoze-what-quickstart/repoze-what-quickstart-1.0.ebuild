# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Ebuild generated by g-pypi 0.2.2 (rev. 214)

inherit distutils

MY_PN="${PN/-/.}"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="The repoze.what Quickstart plugin"
HOMEPAGE="http://code.gustavonarea.net/repoze.what-quickstart/"
SRC_URI="http://pypi.python.org/packages/source/r/${MY_PN}/${MY_P}.tar.gz"
LICENSE="Repoze"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND=">=dev-python/repoze-what-1.0.3
	dev-python/repoze-who
	>=dev-python/repoze-who-plugins-sa-1.0_rc1
	>=dev-python/repoze-what-plugins-sql-1.0_rc1
	dev-python/repoze-who-friendlyform"
DEPEND=""

S="${WORKDIR}/${MY_P}"
