# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $


inherit distutils

MY_PN="${PN/-/.}"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Web Widgets for building and validating forms. (former ToscaWidgetsForms)"
HOMEPAGE="http://toscawidgets.org"
SRC_URI="http://pypi.python.org/packages/source/t/${MY_PN}/${MY_P}.tar.gz"
LICENSE="MIT"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND=">=dev-python/toscawidgets-0.9.3
	>=dev-python/formencode-1.1
	dev-python/mako
	>=dev-python/genshi-0.3.6"
DEPEND=""

S="${WORKDIR}/${MY_P}"
