# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Ebuild generated by g-pypi 0.2.2 (rev. 214)

inherit distutils

MY_PN="repoze.who.plugins.sa"
MY_PV="${PV/_rc/rc}"
MY_P="${MY_PN}-${MY_PV}"

DESCRIPTION="The repoze.who SQLAlchemy plugin"
HOMEPAGE="http://code.gustavonarea.net/repoze.who.plugins.sa/"
SRC_URI="http://pypi.python.org/packages/source/r/${MY_PN}/${MY_P}.tar.gz"
LICENSE="Repoze"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="dev-python/repoze-who
	>=dev-python/sqlalchemy-0.5.0_rc4"
DEPEND=""

S="${WORKDIR}/${MY_P}"

src_install() {
	distutils_src_install

	rm -Rf "${D}/$(python_get_sitedir)"/tests || die "rm failed"
}
