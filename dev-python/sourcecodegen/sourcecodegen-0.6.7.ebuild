# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# Ebuild generated by g-pypi 0.2.2 (rev. 214)

inherit distutils

DESCRIPTION="A Python source-code generator based on the ``compiler.ast`` abstract syntax tree."
HOMEPAGE="UNKNOWN"
SRC_URI="http://pypi.python.org/packages/source/s/${PN}/${P}.tar.gz"
LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""
